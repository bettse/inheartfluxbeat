//
//  NotificationView.swift
//  InHeartFluxBeat WatchKit Extension
//
//  Created by Eric Betts on 9/18/20.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
