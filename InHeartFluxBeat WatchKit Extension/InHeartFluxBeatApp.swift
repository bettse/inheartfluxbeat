//
//  InHeartFluxBeatApp.swift
//  InHeartFluxBeat WatchKit Extension
//
//  Created by Eric Betts on 9/18/20.
//

import SwiftUI

@main
struct InHeartFluxBeatApp: App {
    @WKExtensionDelegateAdaptor(ExtensionDelegate.self) var delegate
    
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView().environmentObject(DataStore.shared)
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
