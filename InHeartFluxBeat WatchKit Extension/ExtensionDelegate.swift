//
//  ExtensionDelegate.swift
//  InHeartFluxBeat WatchKit Extension
//
//  Created by Eric Betts on 9/22/20.
//

import WatchKit

class ExtensionDelegate: NSObject, WKExtensionDelegate {
    private(set) static var singleton: ExtensionDelegate!

    func applicationDidFinishLaunching() {
        print("Application did finish launching")
        ExtensionDelegate.singleton = self
        DataStore.shared.start()
    }

    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
        for task in backgroundTasks {
            switch task {
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
                DataStore.shared.startHeartRateQuery(quantityTypeIdentifier: .heartRate)
                scheduleNextReload()
                backgroundTask.setTaskCompletedWithSnapshot(false)
            default:
                task.setTaskCompletedWithSnapshot(false)
            }
        }
    }

    func scheduleNextReload() {
        let targetDate = nextReloadTime(after: Date())
        NSLog("ExtensionDelegate: scheduling next update at %@", "\(targetDate)")
        WKExtension.shared().scheduleBackgroundRefresh(withPreferredDate: targetDate, userInfo: nil, scheduledCompletion: { _ in })
    }

    private func nextReloadTime(after date: Date) -> Date {
        let calendar = Calendar(identifier: .gregorian)
        let targetMinutes = DateComponents(minute: 15)

        var nextReloadTime = calendar.nextDate(after: date, matching: targetMinutes,matchingPolicy: .nextTime)!

        // If it's in less than 5 minutes, then skip this one and try next hour
        if nextReloadTime.timeIntervalSince(date) < 5 * 60 {
            nextReloadTime.addTimeInterval(60 * 60)
        }

        return nextReloadTime
    }
}
