//
//  ContentView.swift
//  InHeartFluxBeat WatchKit Extension
//
//  Created by Eric Betts on 9/18/20.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var dataStore: DataStore

    var body: some View {
        VStack{
            HStack{
                Text("❤️").font(.system(size: 50))
                Spacer()
            }

            HStack{
                Text("\(dataStore.value)")
                    .fontWeight(.regular)
                    .font(.system(size: 70))

                Text("BPM")
                    .font(.headline)
                    .fontWeight(.bold)
                    .foregroundColor(Color.red)
                    .padding(.bottom, 28.0)
                Spacer()
            }
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(DataStore.shared)
    }
}
