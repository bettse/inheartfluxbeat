//
//  DataStore.swift
//  InHeartFluxBeat WatchKit Extension
//
//  Created by Eric Betts on 9/22/20.
//

import SwiftUI
import HealthKit

class DataStore : NSObject, ObservableObject {
    static let shared = DataStore()

    private var healthStore = HKHealthStore()
    let heartRateQuantity = HKUnit(from: "count/min")
    let influxdb = InfluxData()

    @Published private(set) var value: Int = 0
    
    func start() {
        print("DataStore.start")
        influxdb.setPort(port: 443)
        influxdb.setServer(server: "us-west-2-1.aws.cloud2.influxdata.com")
        influxdb.setBucket(bucket: "bettse%27s+Bucket")
        influxdb.setOrg(org: "9b84587f4c1692c7")
        influxdb.setToken(token: "oZb09py7Mk9uoO7YpzSZyn1Zf_saE9oF62qLWYPj4ut_5f_P4uQEGVasv-zvcyz7Oh0ImV4b--6qmVslgu-qxg==")
        influxdb.setPrecision(precision: DataPrecision.ms)
        print("Configured to send to \(URL(string: influxdb.getConfig())!)")

        let healthKitTypes: Set = [HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!]
        healthStore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { _, _ in
            self.startHeartRateQuery(quantityTypeIdentifier: .heartRate)
            WKExtension.shared().scheduleBackgroundRefresh(withPreferredDate: Date(), userInfo: nil, scheduledCompletion: { _ in })
        }
    }

    func startHeartRateQuery(quantityTypeIdentifier: HKQuantityTypeIdentifier) {
        let sampleType = HKObjectType.quantityType(forIdentifier: quantityTypeIdentifier)!
        let devicePredicate = HKQuery.predicateForObjects(from: [HKDevice.local()])

        let resultsHandler: (HKAnchoredObjectQuery, [HKSample]?, [HKDeletedObject]?, HKQueryAnchor?, Error?) -> Void = { query, samples, deletedObjects, queryAnchor, error in
            // print("resultsHandler")
            guard let samples = samples as? [HKQuantitySample] else {
                return
            }
            if quantityTypeIdentifier == .heartRate {
                self.processResult(samples)
            }
        }

        let updateHandler: (HKAnchoredObjectQuery, [HKSample]?, [HKDeletedObject]?, HKQueryAnchor?, Error?) -> Void = { query, samples, deletedObjects, queryAnchor, error in
            // print("updateHandler")
            guard let samples = samples as? [HKQuantitySample] else {
                return
            }
            if quantityTypeIdentifier == .heartRate {
                self.processUpdate(samples)
            }
        }

        let query = HKAnchoredObjectQuery(type: sampleType, predicate: devicePredicate, anchor: nil, limit: HKObjectQueryNoLimit, resultsHandler: resultsHandler)
        query.updateHandler = updateHandler

        healthStore.execute(query)
    }

    private func processResult(_ samples: [HKQuantitySample]) {
        print("processResult \(samples.count) samples")
        var lastHeartRate = 0.0
        let wki = WKInterfaceDevice.current()

        for sample in samples {
            lastHeartRate = sample.quantity.doubleValue(for: heartRateQuantity)
            let point: Influx = Influx(measurement: "heartrate")
            point.addTag(name: "name", value: wki.name)
            point.setTime(sample.endDate)
            if !point.addValue(name: "bpm", value: Float(lastHeartRate)) {
                print("Invalid value type!")
            }
            influxdb.prepare(point: point)
            DispatchQueue.main.async {
                self.value = Int(lastHeartRate)
            }
        }
        if influxdb.writeBatch() {
            print("Batch written successfully!\n")
        }
    }

    private func processUpdate(_ samples: [HKQuantitySample]) {
        print("processUpdate \(samples.count) samples")
        var lastHeartRate = 0.0
        let wki = WKInterfaceDevice.current()

        for sample in samples {
            lastHeartRate = sample.quantity.doubleValue(for: heartRateQuantity)

            let point: Influx = Influx(measurement: "heartrate")
            point.addTag(name: "name", value: wki.name)
            point.setTime(sample.endDate)
            if !point.addValue(name: "bpm", value: Float(lastHeartRate)) {
                print("Invalid value type!")
            }
            print("\(point.toString(DataPrecision.ms))")
            influxdb.writeSingle(dataPoint: point)
            DispatchQueue.main.async {
                self.value = Int(lastHeartRate)
            }
        }
    }
}
